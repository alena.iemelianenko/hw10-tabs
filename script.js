let tabs = document.querySelectorAll('.tabs-title');
let tabContent = document.querySelectorAll('.tabs-content li');

tabs.forEach(tab => {
    tab.addEventListener('click', activeTab)
})

function activeTab(event){
    tabs.forEach(tab => {
    if (tab === event.target) {
        tab.classList.toggle('active');

        tabContent.forEach(cont => {
           if(cont.getAttribute('data-name') == tab.getAttribute('data-name'))
           cont.classList.toggle('active'); 
           else {
            cont.classList.remove('active');
           }
        });

    } else {
        tab.classList.remove('active');
    } 
});
};
